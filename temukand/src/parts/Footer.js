import React from "react";

import Button from "elements/Button";
import IconText from "parts/IconText";
export default function Footer() {
  return (
    <footer>
      <div className="container">
        <div className="row">
          <div className="col-auto" style={{ width: 350 }}>
            <IconText />
            <p className="brand-tagline">
              We kaboom your beauty holiday instantly and memorable. 
              Kami 
            </p>
          </div>
          <div className="col-auto mr-5">
            <h6 className="mt-2">Untuk Pemula</h6>
            <ul className="list-group list-group-flush">
              <li className="list-group-item">
                <Button type="link" href="/register">
                  Akun Baru
                </Button>
              </li>
              <li className="list-group-item">
                <Button type="link" href="/properties">
                  Mulai Cari
                </Button>
              </li>
              <li className="list-group-item">
                <Button type="link" href="/use-payments">
                  Pembayaran
                </Button>
              </li>
            </ul>
          </div>
          <div className="col-2 mr-5">
            <h6 className="mt-2">Explore Us</h6>
            <ul className="list-group list-group-flush">
              <li className="list-group-item">
                <Button type="link" href="/careers">
                  Lowongan
                </Button>
              </li>
              <li className="list-group-item">
                <Button type="link" href="/privacy">
                  Privasi
                </Button>
              </li>
              <li className="list-group-item">
                <Button type="link" href="/terms">
                  Syarat & Ketentuan
                </Button>
              </li>
            </ul>
          </div>
          <div className="col-3">
            <h6 className="mt-2">Hubungi Kami</h6>
            <ul className="list-group list-group-flush">
              <li className="list-group-item">
                <Button isExternal type="link" href="#">
                  support@temukand.id
                </Button>
              </li>
              <li className="list-group-item">
                <Button isExternal type="link" href="tel:+622122081996">
                  0899 6881 537
                </Button>
              </li>
              <li className="list-group-item">
                <span>Temukand, Bandung</span>
              </li>
            </ul>
          </div>
        </div>
        <div className="row">
          <div className="col text-center copyrights">
            Copyright 2021 • All rights reserved • Temukand
          </div>
        </div>
      </div>
    </footer>
  );
}
